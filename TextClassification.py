"""
====================================================================================
Compare text classification performance of Naive Bayes and SVM, generate ROC curves
====================================================================================

Author: Haotian Shi, 2017

"""
#print(__doc__)

import time
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score, precision_score, recall_score, roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.pipeline import Pipeline

# generate train data and test data
twenty_train = fetch_20newsgroups(data_home='data/twenty_newsgroups', subset='train', shuffle=True, random_state=20)
twenty_test = fetch_20newsgroups(data_home='data/twenty_newsgroups', subset='test', shuffle=True, random_state=30)

tfidf_vectorizer = TfidfVectorizer(ngram_range=(1,2),stop_words='english',lowercase=True,norm='l2',sublinear_tf=True)
X_train_tfidf = tfidf_vectorizer.fit_transform(twenty_train.data) #transform X_train to a normalized tf-idf representation
y_train = twenty_train.target
X_test_tfidf = tfidf_vectorizer.transform(twenty_test.data)
y_test = twenty_test.target

# optimize MultinomialNB model and SVM model
MultinomialNB_metrics = dict()
SVM_metrics = dict()
MultinomialNB_clf = MultinomialNB()
SVM_clf = SVC(kernel=cosine_similarity)
MultinomialNB_parameters = {'alpha':(0.005, 0.007, 0.009)}
SVM_parameters = {'C':(1.0, 2.0, 2.2, 3.0), 'gamma':(1e-7, 1e-6, 1e-5)}

MultinomialNB_tfidf_gs_clf = GridSearchCV(MultinomialNB_clf, MultinomialNB_parameters, cv=2)
train_start_time =  time.time()
MultinomialNB_tfidf_gs_clf.fit(X_train_tfidf, y_train)
MultinomialNB_metrics['train time'] = time.time() - train_start_time
#print MultinomialNB_tfidf_gs_clf.best_params_

SVM_tfidf_gs_clf = GridSearchCV(SVM_clf, SVM_parameters, cv=2)
train_start_time =  time.time()
SVM_tfidf_gs_clf.fit(X_train_tfidf, y_train)
SVM_metrics['train time'] = time.time() - train_start_time
#print SVM_tfidf_gs_clf.best_params_

# print table of metrics
y_train_predict_MultinomialNB = MultinomialNB_tfidf_gs_clf.predict(X_train_tfidf)
y_test_predict_MultinomialNB = MultinomialNB_tfidf_gs_clf.predict(X_test_tfidf)
MultinomialNB_metrics['train accuracy'] = accuracy_score(y_train, y_train_predict_MultinomialNB)
MultinomialNB_metrics['test accuracy'] = accuracy_score(y_test, y_test_predict_MultinomialNB)
MultinomialNB_metrics['train precision'] = precision_score(y_train, y_train_predict_MultinomialNB, average='weighted')
MultinomialNB_metrics['test precision'] = precision_score(y_test, y_test_predict_MultinomialNB, average='weighted')
MultinomialNB_metrics['train recall'] = recall_score(y_train, y_train_predict_MultinomialNB, average='weighted')
MultinomialNB_metrics['test recall'] = recall_score(y_test, y_test_predict_MultinomialNB, average='weighted')

y_train_predict_SVM = SVM_tfidf_gs_clf.predict(X_train_tfidf)
y_test_predict_SVM = SVM_tfidf_gs_clf.predict(X_test_tfidf)
SVM_metrics['train accuracy'] = accuracy_score(y_train, y_train_predict_SVM)
SVM_metrics['test accuracy'] = accuracy_score(y_test, y_test_predict_SVM)
SVM_metrics['train precision'] = precision_score(y_train, y_train_predict_SVM, average='weighted')
SVM_metrics['test precision'] = precision_score(y_test, y_test_predict_SVM, average='weighted')
SVM_metrics['train recall'] = recall_score(y_train, y_train_predict_SVM, average='weighted')
SVM_metrics['test recall'] = recall_score(y_test, y_test_predict_SVM, average='weighted')

print '\t\t     MultinomialNB\t     SVM'
for metric in ['train accuracy', 'test accuracy', 'train precision', 'test precision', 'train recall', 'test recall', 'train time']:
    print '{0:20}{1:10}\t{2:10}'.format(metric, MultinomialNB_metrics[metric], SVM_metrics[metric])

# generate ROC curve for specific classes
classes = {1:'comp.graphics', 4:'comp.sys.mac.hardware', 8:'rec.motorcycles', 14:'sci.space', 17:'talk.politics.mideast'}
y_train = label_binarize(y_train, classes=list(range(20)))
y_test = label_binarize(y_test, classes=list(range(20)))

MultinomialNB_multiclass_clf = OneVsRestClassifier(MultinomialNB(alpha=MultinomialNB_tfidf_gs_clf.best_params_['alpha']))
MultinomialNB_multiclass_clf.fit(X_train_tfidf, y_train)

SVM_multiclass_clf = OneVsRestClassifier(SVC(kernel=cosine_similarity, probability=True, C=SVM_tfidf_gs_clf.best_params_['C'], gamma=SVM_tfidf_gs_clf.best_params_['gamma']))
SVM_multiclass_clf.fit(X_train_tfidf, y_train)

y_test_prob_MultinomialNB = MultinomialNB_multiclass_clf.predict_proba(X_test_tfidf)
fpr_MultinomialNB = dict()
tpr_MultinomialNB = dict()
roc_auc_MultinomialNB = dict()
for clazz in classes.keys():
    fpr_MultinomialNB[clazz], tpr_MultinomialNB[clazz], thre = roc_curve(y_test[:, clazz], y_test_prob_MultinomialNB[:, clazz])
    roc_auc_MultinomialNB[clazz] = auc(fpr_MultinomialNB[clazz], tpr_MultinomialNB[clazz])

y_test_prob_SVM = SVM_multiclass_clf.predict_proba(X_test_tfidf)
fpr_SVM = dict()
tpr_SVM = dict()
roc_auc_SVM = dict()
for clazz in classes.keys():
    fpr_SVM[clazz], tpr_SVM[clazz], thre = roc_curve(y_test[:, clazz], y_test_prob_SVM[:, clazz])
    roc_auc_SVM[clazz] = auc(fpr_SVM[clazz], tpr_SVM[clazz])

colors_MultinomialNB = ['dodgerblue', 'c', 'lightgreen', 'olive', 'blue']
colors_SVM = ['orange', 'red', 'darkviolet', 'gray', 'lime']
plt.figure()
for clazz, color in zip(classes.keys(), colors_MultinomialNB):
    plt.plot(fpr_MultinomialNB[clazz], tpr_MultinomialNB[clazz], color=color, lw=1, label='{0}, MultinomialNB (auc = {1:0.2f})'.format(classes[clazz], roc_auc_MultinomialNB[clazz]))

for clazz, color in zip(classes.keys(), colors_SVM):
    plt.plot(fpr_SVM[clazz], tpr_SVM[clazz], color=color, lw=1, label='{0}, SVM (auc = {1:0.2f})'.format(classes[clazz], roc_auc_SVM[clazz]))

plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curves for specific classes')
plt.legend(loc='lower right')
plt.savefig('TextClassifierROC.pdf')

